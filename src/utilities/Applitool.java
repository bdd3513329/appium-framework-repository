package utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.applitools.eyes.BatchInfo;
import com.applitools.eyes.RectangleSize;
import com.applitools.eyes.exceptions.DiffsFoundException;
import com.applitools.eyes.selenium.Eyes;
import com.applitools.eyes.selenium.fluent.Target;

import io.appium.java_client.ios.IOSDriver;

public class Applitool extends Configuration{

		public static Eyes eyes;
	
	//----Initialize Batch
		public void initializeBatch(String batchName){
			try {
				String myEyesServer = "https://applitools-openreach.nat.bt.com/"; 
				eyes = new Eyes();
				BatchInfo batch=new BatchInfo(batchName);
				eyes.setBatch(batch);
				//eyes.setForceFullPageScreenshot(true);					
				eyes.setApiKey("k2VTMt4vJC2nc2H9oEOg2K8BSZvsSGc9nUk102vK9ldJU110");
//				eyes.setServerUrl(myEyesServer);
//				eyes.open(driver, "MyServices", batchName,new RectangleSize(1268, 638));
				
				eyes.setServerUrl(myEyesServer);
				eyes.setMatchTimeout(0);  // new line added
				eyes.setSendDom(false);   // new line added
				eyes.open(driver, "MyServices", batchName);
				System.out.println("eyes open");
				
				
			} catch (Exception e) {
				System.out.println(e);
			}
		}
	/**
	  * Capture full page with check point image name.
	  */	
		public void verifyWindow(String checkImageName) throws Exception {		
			try {
				eyes.checkWindow(checkImageName);
				eyes.setForceFullPageScreenshot(true); 
			} 
			catch (IllegalStateException e) {
				System.out.println(e);
			}				
		}
		
	/**
	  * Capture only target popup window with check point image name.
	  * You have to provide target popup window element location
	  */	
			public void verifyWindow(String checkImageName,WebElement element) throws Exception {		
				try {
					eyes.check(checkImageName, Target.region(element));
					
				} 
				catch (IllegalStateException e) {
					System.out.println(e);
				}				
			}
	//--Close Eyes	
		public void closeEyes()		{
			try 
			{
				eyes.close();				
			}
			catch (DiffsFoundException e) {
				System.out.println("Checkpoint Image is not matched with basline image in Applitool server");
				
			}
		}
}
