
package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
//import com.sun.org.apache.bcel.internal.generic.Select;

import cucumber.api.Scenario;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class Configuration extends wrapperActionTest {
	public DesiredCapabilities capabilities;
	public static IOSDriver<?> driver;
	public static AndroidDriver<AndroidElement> androidDriver;
	public static String serverName;
	public static String serverPort;
	public static String serverIP;
	public static WebDriver driver1;
	public String testReportName;
	public static Properties prop;
	public static XtentReport report;
	
	public Configuration(){

		try {
			prop = new Properties();
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "\\src\\utilities\\config.properties");
			prop.load(fis);
		} catch (IOException e) {
			e.getMessage();
		}
	}
	
	public void getDeviceConnection() {
		try {

			capabilities = new DesiredCapabilities();
			capabilities.setCapability("report.disable", true);
			capabilities.setCapability("instrumentApp", true);
			capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, serverName);
			capabilities.setCapability("autoAcceptAlerts",true);
			driver = new IOSDriver<>(new URL("http://localhost:4723/wd/hub"), capabilities);
			// driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"),
			// capabilities);
			driver.executeScript("client:client.setProperty('ios.non-instrumented.dump.parameters', '20,1000,50')");
			driver.executeScript("client:client.deviceAction(\"Home\")");
			
			driver.setLogLevel(Level.INFO);
			

		} catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
	}
	
	
	public void initializeReport(String testName) {
		try {

			testReportName = testName.replace(" ","_");
			report = XtentReport.getReportInstance();
			report.setDriver(driver);
			report.setTestName(testReportName);
			report.startReport();
		} catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
	}
    

	public void launchApplication(WebElement element) {
		try {
			int reRunFlag = 0;
			while (reRunFlag <= 14) {

				if (element.isDisplayed()) {
					System.out.println("App presence located on iphone");
					break;
				} else {
					System.out.println("App is not yet seen on the screen");

				}
				reRunFlag++;
			}

			if (reRunFlag == 15) {
				System.out.println("Application not installed on the device");
				driver.quit();
			}

			WebDriverWait wait = new WebDriverWait(driver, 60);

			wait.until(ExpectedConditions.elementToBeClickable(element));
			element.click();
			((JavascriptExecutor) driver).executeScript("client:client.report(\"Successfully launched App\", true)",
					"");

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}

	}

	public boolean swipeUp() {
		try {
			Thread.sleep(3000);
			driver.executeScript("client:client.swipe(\"DOWN\", 800, 1000)");

		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	public boolean swipeUpValue(int x, int y) {
		try {
			Thread.sleep(3000);
			driver.executeScript("client:client.swipe(\"DOWN\", "+x+", "+y+")");

		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	public boolean swipeDown() {
		try {
			Thread.sleep(3000);
			driver.executeScript("client:client.swipe(\"UP\", 300, 500)");
			Thread.sleep(2000);
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	public boolean slideDown() {
		try {
			Thread.sleep(3000);
			driver.executeScript("client:client.swipe(\"UP\", 800, 500)");
			Thread.sleep(2000);
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	public boolean swipeLeft() {
		try {
			Thread.sleep(3000);
			driver.executeScript("client:client.swipe(\"LEFT\", 800, 1000)");

		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	public void scrollForElementVisible(WebElement element) {
		try {
			while (!element.isDisplayed()) {
				swipeUp();
				swipeUp();
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	
	public String readPropertyFileData(String getValue) throws IOException
	{
		
		InputStream input = new FileInputStream(System.getProperty("user.dir") + File.separator + "src"
				+ File.separator + "utilities" + File.separator + "config.properties");
		Properties prop = new Properties();
		prop.load(input);
		return prop.getProperty(getValue);
	}
	

}
