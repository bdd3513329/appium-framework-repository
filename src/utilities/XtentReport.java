package utilities;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class XtentReport {

	public static XtentReport report = new XtentReport();
	public static ExtentReports eReport;
	public static ExtentTest logger;
	private WebDriver driver;
	private String testName;
	private String reportFolder;
	private String timeStamp;
	private String reportFile;
	public String screenshotFolder;
	
	
	  public XtentReport() {
	  
	  }
	 

	public XtentReport startReport() {
		setReportPath();
		timeStamp = fn_getDateTime();
		eReport = new ExtentReports();
		ExtentHtmlReporter reporter = new ExtentHtmlReporter(reportFile);
		eReport.attachReporter(reporter);
		logger = eReport.createTest(testName);
		logger.assignAuthor("Tejaswini Deshmukh");
		reporter.config().setTheme(Theme.DARK);
		reporter.config().setDocumentTitle("A");
		reporter.config().setReportName("OpenReach Test Automation - MyServices");
		eReport.setSystemInfo("User Name", System.getProperty("user.name"));
		eReport.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
		eReport.setSystemInfo("Machine", "Windows 10" +" "+ "64 Bit");
		eReport.setSystemInfo("Selenium", "3.7.0");
		//eReport.setSystemInfo("Maven", "3.5.2");
		eReport.setSystemInfo("Java Version", System.getProperty("java.version"));
		return this;
	}
 
//	public String getReportfolder() {
//		timeStamp = fn_getDateTime();
//		System.out.println("getReportfolder fun testName - " +testName );
//		String scenariosTestName = testName;
//		reportFolder = System.getProperty("user.dir") + File.separator + "Reports" + File.separator + testName + "_"
//					+ timeStamp;
//		System.out.println("reportFolder0 "+ reportFolder);
//		return reportFolder;
//	}
	
	public void setReportPath() {
		try {

			timeStamp = fn_getDateTime();
//			System.out.println("ReportPath fun testName - " +testName );
//			System.out.println("ReportPath fun testReportName - ");
//			reportFolder = System.getProperty("user.dir") + File.separator + "Reports" + File.separator + testName + "_"
//					+ timeStamp;
			reportFolder = System.getProperty("user.dir") + File.separator + "Reports";
			
			reportFile = reportFolder + File.separator + testName + "_" + timeStamp + ".html";
			
//			screenshotFolder = reportFolder + File.separator + "screenshots";
//			File screensFdr = new File(screenshotFolder);
//			screensFdr.mkdirs();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public String fn_getDateTime() {
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH-mm-ss");
			Date date = Calendar.getInstance().getTime();
			String currentDate = dateFormat.format(date);
			return currentDate;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}



	public void fn_takeScreenShot(WebDriver driver,String fileName) {
		String capturePath = "";
		try {
			TakesScreenshot scrShot = ((TakesScreenshot) driver);
			File srcFile = scrShot.getScreenshotAs(OutputType.FILE);
//			System.out.println("takeCSreenshot fun testName - " + testName);
//			File destFile = new File(System.getProperty("user.dir") + File.separator + "Reports" + File.separator + testName + File.separator + "screenshots" + fileName.replaceAll("[^a-zA-Z0-9]", "_") + "_"
//					+ fn_getDateTime() + ".jpg");  //tried this way but not working
			
			File destFile = new File(System.getProperty("user.dir") + File.separator +"screenshotFolder" + File.separator + fileName.replaceAll("[^a-zA-Z0-9]", "_") + "_"
					+ fn_getDateTime() + ".jpg");
			FileUtils.copyFile(srcFile, destFile);
			 capturePath = destFile.getAbsolutePath();
			 logger.addScreenCaptureFromPath(capturePath);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			reportWarningEvent("Unable to capture the screenshot");
		}
	}

	public WebDriver getDriver() {
		return driver;
	}

	public String getTestName() {
		return testName;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public static XtentReport getReportInstance() {
		if (report == null)
			return new XtentReport();
		return report;
	}

	public void reportPassEvent(WebDriver driver,String stepName) {
		logger.log(Status.PASS, stepName);
		fn_takeScreenShot(driver,stepName);
	}

	public void reportInfoEvent(WebDriver driver,String stepName) {
		logger.log(Status.INFO, stepName);
		fn_takeScreenShot(driver,stepName);
	}

	public void reportFailEvent(WebDriver driver,String stepName) {
		logger.log(Status.FAIL, stepName);
		fn_takeScreenShot(driver,stepName);
		Assert.fail(stepName);
	}

	public void reportWarningEvent(String stepName) {
		logger.log(Status.WARNING, stepName);
		Assert.fail(stepName);
	}

	public void endReport() {
		try {
			eReport.flush();
		} catch (Exception ex) {
			ex.getMessage();
			ex.printStackTrace();
		}
	}

}
