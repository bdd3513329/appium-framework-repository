package utilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class ApplicationUtility {
	public static Logger LOGGER = Logger.getLogger(ApplicationUtility.class.getSimpleName());
	public static RemoteWebDriver driver;
	public static DesiredCapabilities capabilities;

	public void logMessage(String string, String string2, String string3, String string4) {
		LOGGER.log(Level.INFO,
				"Action: " + string + "\tExpected: " + string2 + "\tActual: " + string3 + "\tStatus: " + string4);
	}

	public void browserInitiate(String browser, String url) throws MalformedURLException {
		if (browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver",
			System.getProperty("user.dir") + "\\Library\\chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			driver = new ChromeDriver(options);
			driver.navigate().to(url);

			// capabilities.setCapability("chrome.switches", Arrays.asList("--proxy
			// \"http=http://proxy.intra.bt.com:8080/;https=http://proxy.intra.bt.com:8080/\""));
			// capabilities = DesiredCapabilities.chrome();
			// capabilities.setCapability("version", "");
			// capabilities.setCapability("platform","LINUX");
			//
			//
			// driver = new RemoteWebDriver(new URL("http://10.154.15.153:4444/wd/hub"),
			// capabilities);
			// //http://172.17.0.3:5555/wd/hub
			//
			// driver.manage().deleteAllCookies();
			// driver.navigate().to("http://www.google.com");
			// System.out.println(driver.getTitle());
		} else if (browser.equalsIgnoreCase("IE")) {
			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			capabilities.setCapability("requireWindowFocus", true);
			driver = new InternetExplorerDriver(capabilities);
			driver.manage().deleteAllCookies();
			driver.manage().window().maximize();
			driver.navigate().to(url);
		}
	}
}
