package utilities;

import org.openqa.selenium.WebDriver;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.ios.IOSDriver;

public class wrapperActionTest extends objectDefinitionLibrary {
	protected ApplicationUtility appUtility = new ApplicationUtility();
	XtentReport report = new XtentReport();
	

	/* =================================================================================
	* =================================================================================
	*  Function Description: findElement
	*  Parameters: Return Value: Created On: Updated On: Update Reason:
	* =================================================================================
	* =================================================================================
	*/
	public WebElement findElement(WebDriver driver, String locator, String objectName) {
		WebElement webelement = null;
		if (locator != null) {
			String[] arrLocator = locator.split("==");
			String locatorTag = arrLocator[0].trim();
			String objectLocator = arrLocator[1].trim();
			try {
				if (locatorTag.equalsIgnoreCase("id")) {
					webelement = driver.findElement(By.id(objectLocator));
				} else if (locatorTag.equalsIgnoreCase("name")) {
					webelement = driver.findElement(By.name(objectLocator));
				} else if (locatorTag.equalsIgnoreCase("xpath")) {
					webelement = driver.findElement(By.xpath(objectLocator));
				} else if (locatorTag.equalsIgnoreCase("linkText")) {
					webelement = driver.findElement(By.linkText(objectLocator));
				} else if (locatorTag.equalsIgnoreCase("class")) {
					webelement = driver.findElement(By.className(objectLocator));
				} else if (locatorTag.equalsIgnoreCase("tagname")) {
					webelement = driver.findElement(By.tagName(objectLocator));
				}

			}

			catch (UnhandledAlertException e) {
				String errmsg = e.getMessage();
				if (errmsg.length() > 500) {
					errmsg = errmsg.substring(0, 500);
				}
				appUtility.logMessage("Test Execution Failed", "Exception Occurred", "Exception Occurred " + errmsg,
						"fail");
				Assert.assertTrue(false);

			}

			catch (NoSuchElementException e) {
				String errmsg = e.getMessage();
				if (errmsg.length() > 500) {
					errmsg = errmsg.substring(0, 500);
				}
				appUtility.logMessage("Test Execution Failed", "Exception Occurred", "Exception Occurred " + errmsg,
						"fail");
				Assert.assertTrue(false);

			}

			catch (NoSuchFrameException e) {
				String errmsg = e.getMessage();
				if (errmsg.length() > 500) {
					errmsg = errmsg.substring(0, 500);
				}
				appUtility.logMessage("Test Execution Failed", "Exception Occurred", "Exception Occurred " + errmsg,
						"fail");
				Assert.assertTrue(false);

			}

			catch (Error e) {

				String errmsg = e.getMessage();
				if (errmsg.length() > 500) {
					errmsg = errmsg.substring(0, 500);
				}
				appUtility.logMessage("Test Execution Failed", "Exception Occurred", "Exception Occurred " + errmsg,
						"fail");
				Assert.assertTrue(false);
			}
		}
		return webelement;
	}
	
/* =================================================================================
	 * =================================================================================
	 *  Function Description: enterText using WebElement
	 *  Parameters: Return Value: Created On: Updated On: Update Reason:
	 * =================================================================================
	 * =================================================================================
	 */
	public boolean enterText(WebDriver driver, WebElement element, String text, String objectName)
			throws Exception {

		try {
			Actions act = new Actions(driver);
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.visibilityOf(element));

			if (element != null) {
				element.clear();
				element.click();
				element.clear();
				act.sendKeys(text).build().perform();
				appUtility.logMessage("Input Text",
						objectName + " should be present in the page and click successfully ",
						objectName + " is present in the page and clicked successfully", "Passed");
				report.reportPassEvent(driver, text+" text entered on  "+objectName);
				return true;
			} else {
				appUtility.logMessage("Input Text",
						objectName + " should be present in the page and click successfully ",
						objectName + " is not present in the page and not clicked", "Failed");
				report.reportFailEvent(driver, objectName + "failed");
				return false;
			}
		}

		catch (UnhandledAlertException e) {
			String errmsg = e.getMessage();
			if (errmsg.length() > 500) {
				errmsg = errmsg.substring(0, 500);
			}
			appUtility.logMessage("Test Execution Failed", "Exception Occurred", "Exception Occurred " + errmsg,
					"fail");
			return false;

		}

		catch (NoSuchElementException e) {
			String errmsg = e.getMessage();
			if (errmsg.length() > 500) {
				errmsg = errmsg.substring(0, 500);
			}
			appUtility.logMessage("Test Execution Failed", "Exception Occurred", "Exception Occurred " + errmsg,
					"fail");
			return false;

		}

		catch (NoSuchFrameException e) {
			String errmsg = e.getMessage();
			if (errmsg.length() > 500) {
				errmsg = errmsg.substring(0, 500);
			}
			appUtility.logMessage("Test Execution Failed", "Exception Occurred", "Exception Occurred " + errmsg,
					"fail");
			return false;

		}

		catch (Error e) {

			String errmsg = e.getMessage();
			if (errmsg.length() > 500) {
				errmsg = errmsg.substring(0, 500);
			}
			appUtility.logMessage("Test Execution Failed", "Exception Occurred", "Exception Occurred " + errmsg,
					"fail");
			return false;
		}

	}


	/* =================================================================================
	 * =================================================================================
	 *  Function Description: Click on element
	 *  Parameters: Return Value: Created On: Updated On: Update Reason:
	 * =================================================================================
	 * =================================================================================
	 */
	public boolean clickIosElement(WebDriver driver, WebElement element, String objectName) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.elementToBeClickable(element));

			if (element != null) {
				element.click();
				appUtility.logMessage("Click on Element",
						objectName + " should be present in the page and click successfully ",
						objectName + " is present in the page and clicked successfully", "Passed");
				report.reportPassEvent(driver, "clicked on "+objectName);
				return true;
			} else {
				appUtility.logMessage("Click on Element",
						objectName + " should be present in the page and click successfully ",
						objectName + " is not present in the page and not clicked", "Failed");
				report.reportFailEvent(driver, objectName + "failed");
				return false;
			}
		} catch (Exception ex) {
			System.out.println(ex);
			report.reportFailEvent(driver, objectName + "failed");
			return false;
		}
	}

	/* =================================================================================
	 * =================================================================================
	 *  Function Description: check visiblity of element
	 *  Parameters: Return Value: Created On: Updated On: Update Reason:
	 * =================================================================================
	 * =================================================================================
	 */
	public void checkElementVisible(WebDriver driver, WebElement element, String objectName) {
		try {
			Thread.sleep(2000);
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOf(element));
			report.reportPassEvent(driver, "Element visible "+objectName);
		} catch (Exception e) {
			report.reportFailEvent(driver, objectName + " Element Not Visible");

		}
	}

	/* =================================================================================
	 * =================================================================================
	 *  Function Description: verify Text
	 *  Parameters: Return Value: Created On: Updated On: Update Reason:
	 * =================================================================================
	 * =================================================================================
	 */
	public boolean verifyText(WebDriver driver, String field, String objectName, String txt) throws Exception {
		WebElement element = findElement(driver, field, objectName);
		try {
			String value = (element).getText();
			System.out.println(value);
			if (value.equals(txt)) {
				appUtility.logMessage("Text Found", objectName + " should be present in the page ",
						objectName + " is present in the page ", "Passed");

				return true;
			} else {
				appUtility.logMessage("Text Not Found", objectName + " should not be present in the page ",
						objectName + " is not present in the page ", "Failed");
				return false;

			}
		} catch (Exception ex) {
			return false;
		}
	}
	
	/* =================================================================================
	 * =================================================================================
	 *  Function Description: Get text
	 *  Parameters: Return Value: Created On: Updated On: Update Reason:
	 * =================================================================================
	 * =================================================================================
	 */
	
	public String getText(WebDriver driver, WebElement element){
		
		try {
			String value = (element).getText();
			return value;

		} catch (Exception ex) {
			return null;
		}
	}
	
	
	public boolean selectElementByValue (WebDriver driver,WebElement element, String value) {
		try {
			if (element.isDisplayed()) {
				Select select = new Select(element);
				select.selectByIndex(2);
				//report.reportPassEvent(driver, "Element selected "+value);
				return true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} return false;
	}
	
	/* =================================================================================
	 * =================================================================================
	 *  Function Description: switch context
	 *  Parameters: Return Value: Created On: Updated On: Update Reason:
	 * =================================================================================
	 * =================================================================================
	 */
	public void switchContextWebView(IOSDriver driver)
	{
		driver.context("WEBVIEW_1");
	}
	
	public void switchContextNativeView(IOSDriver driver)
	{
		driver.context("NATIVE_APP");
	}
	

}
