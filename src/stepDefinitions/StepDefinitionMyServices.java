package stepDefinitions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import pageObjects.MyServicesStepLocator;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utilities.Applitool;
//import utilities.Applitools;
import utilities.Configuration;

public class StepDefinitionMyServices extends Configuration {
	Scenario scenario;
	MyServicesStepLocator myServicesObj;
	
	@Before // use hooks
	public void setup(Scenario scenario) {
		this.scenario = scenario;
		System.out.println(scenario.getName());
		initializeReport(scenario.getName());
		
		getDeviceConnection();
		myServicesObj = new MyServicesStepLocator();
		myServicesObj.launchSafariBrowser();
		
	}
	
	@Given("User launch and login MyServices app")
	public void user_launch_and_login_MyServices_app() {
		myServicesObj.LoginToMyServicesApp();
	}

	@Then("fetch task details")
	public void fetch_task_details() {
		myServicesObj.fetchTaskDetails();
	}
	
	
	@After // use hooks
	public void afterScenario() {
		report.endReport();
		driver.close();
		driver.quit();
	}

}
