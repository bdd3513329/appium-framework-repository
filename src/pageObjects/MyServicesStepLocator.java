package pageObjects;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.Configuration;

public class MyServicesStepLocator extends Configuration{
	/* =================================================================================
	 * =================================================================================
	 *  Create Relative XPATH for WebElement
	 *  Do not create Absolute XPATH
	 *  Do not create XPATH using indexing or any such locator which need more maintenance work.
	 *  Please follow below example for XPATH creation.
	 * =================================================================================
	 * =================================================================================
	 */
	@FindBy(xpath = "//*[@accessibilityLabel='Safari']")
	WebElement safariBrowser;
	@FindBy(xpath = "//*[@text='Allow']")
	List<WebElement> allowButton;
	@FindBy(xpath = "//*[@name='loginfmt']")
	WebElement azureLoginId;
	@FindBy(xpath = "//*[@value='Next']")
	WebElement nextButton;
	@FindBy(xpath = "//*[@text='Continue']")
	WebElement continueBtn;
	@FindBy(xpath = "//*[@name='passwd']")
	WebElement azureLoginPwd;
	@FindBy(xpath = "//*[@value='Sign in']")
	WebElement signInButton;
	@FindBy(xpath = "//*[@text='Search Job']")
	WebElement searchJobBtn;
	@FindBy(xpath = "//*[@placeholder='Job ID']")
	WebElement jobIdTextBox;
	@FindBy(xpath = "//*[@text='Fetch']")
	WebElement fetchButton;
	@FindBy(xpath = "//*[@text='Service ID']")
	WebElement serviceIdText;
	@FindBy(xpath= "//span[text()='Menu']")
	WebElement menu;
	@FindBy(xpath= "//a[text()='Create Project']")
	WebElement createProject;
	@FindBy(xpath= "//div[text()='Project Create']")
	WebElement projectCreate;
	@FindBy(xpath= "//div[@class='dropdown-menu show']//a[text()='Project Search']")
	WebElement projectSearch;
	@FindBy(xpath= "//span[text()=' Search ']")
	WebElement btnSearch;
	
	String uName =prop.getProperty("goldUser");
	String passWord =prop.getProperty("goldUserPw");
	String jobID =prop.getProperty("taskID");
	
	
	public MyServicesStepLocator() {
		PageFactory.initElements(driver, this);
	}
	
	//launch browser
	public void launchSafariBrowser() {
		launchApplication(safariBrowser);
	}
	
	public void LoginToMyServicesApp(){
		try
		{
		 driver.get(prop.getProperty("myServicesTestUrl"));	
		 driver.context("NATIVE_APP");
			if (allowButton.size() != 0) {
				clickIosElement(driver, allowButton.get(0), "allowButton");
			}
		 driver.context("WEBVIEW_1");	
		 Thread.sleep(3000);
		 enterCredentials(uName , passWord);
		
		}
		catch(Exception e)
		{
			report.reportFailEvent(driver, "Element not found or Error occured");
		}
		

	}
	
	public void enterCredentials(String uname , String pw){
		try {
			enterText(driver, azureLoginId,uname , "azureLoginId");
			clickIosElement(driver,nextButton, "nextButton");
			Thread.sleep(3000);
			enterText(driver, azureLoginPwd, pw, "azureLoginPwd");
			clickIosElement(driver, signInButton, "signInButton");
			driver.context("NATIVE_APP");
			clickIosElement(driver,continueBtn,"continueBtn");
		    driver.context("WEBVIEW_1");
		}catch(Exception e) {
			report.reportFailEvent(driver, "Login not successful");
		}
		
	}
	
	public void fetchTaskDetails(){
		try {
			enterJobId(jobID);
			clickIosElement(driver, fetchButton, "fetchButton");
			Thread.sleep(3000);
			Set<String> contextNames = driver.getContextHandles();
			System.out.println(contextNames);
			driver.context("NATIVE_APP");
			if (allowButton.size() != 0) {
				clickIosElement(driver, allowButton.get(0), "allowButton");
			}
			driver.context("WEBVIEW_1");
			checkElementVisible(driver, serviceIdText, "serviceIdText");
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void enterJobId(String jobId) {
		try
		{
		clickIosElement(driver, searchJobBtn, "searchJobBtn");
		enterText(driver, jobIdTextBox, jobId, "jobIdTextBox");
		}
		catch(Exception e)
		{
			report.reportFailEvent(driver, "Element not found or Error occured");
		}
	}

	

}
