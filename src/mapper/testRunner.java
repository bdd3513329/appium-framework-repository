package mapper;
import org.junit.runner.RunWith;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
				
		features= {"src//features//SampleScenario.feature"} 
		,glue= {"com.stepDefinitions"} 
		,plugin = {"pretty", "html:Reports/cucumber"}
		,monochrome=true
		,dryRun=true
		
	
	)

 	
public  class testRunner
{	
	
}
